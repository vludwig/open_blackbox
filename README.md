# Open the Black-box using model agnostic explanation

This is the repo for the PwC Workshop "Open the Black-box using model agnostic explanation" at the WeAreDevelopers AI Congress 2018 in Vienna.

Prior to the workshop, please make sure that you have Python 3 and pip as well as the following libraries installed: jupyter, sklearn, pandas, lime, ipywidgets, (optionally keras and tensorflow). 

Download and extract the [zip file](open_blackbox.zip)

Open the Terminal/Command Line and navigate to the directory where the files have been extracted to. Run ``` jupyter notebook ``` to start the jupyter server.  

[Basic lime example with random forest](WRD workshop ohe random forest.ipynb)

In case you wish to apply keras with sklearn's grid search, a model building function needs to specified as in the following example:  
[Special lime example with cross-validation and keras](WRD workshop keras cv.ipynb)

